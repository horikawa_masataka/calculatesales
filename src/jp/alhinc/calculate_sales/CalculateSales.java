package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// 支店コードの正規表現
	private static final String BRANCH_REGEX = "^[0-9]{3}$";
	// 商品コードの正規表現
	private static final String COMMODITY_REGEX = "^[A-Za-z0-9]{8}$";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String SERIAL_NUMBER_ERROR = "売上ファイル名が連番になっていません";
	private static final String SALE_AMOUNT_ERROR = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// エラーメッセージ：コマンドライン引数が渡されていない場合
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> productNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> productSales = new HashMap<>();

		// 支店定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "支店" , BRANCH_REGEX)) {
			return;
		}
		// 商品定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, productNames, productSales, "商品", COMMODITY_REGEX)) {
			return;
		}

		// 全てのファイルを取得し、ファイル選定のリストを作成
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		// 正規表現に合致することの確認が取れたファイルのみを上記リストに追加
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		// エラーメッセージ：売上ファイルが連番でない場合
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if((latter - former) != 1) {
				System.out.println(SERIAL_NUMBER_ERROR);
				return;
			}
		}

		// 以降、リストに追加したファイルの中身を1つずつ処理
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				// 売上ファイルの支店コードと売上金額を格納するリストの作成
				List<String> fileContents = new ArrayList<>();

				// 1行ずつ中身を取得してリストに格納
				String line;
				while((line = br.readLine()) != null) {
					fileContents.add(line);
				}

				// リストに格納した各要素：支店コード、商品コード、売上
				String branchCode = fileContents.get(0);
				String productCode = fileContents.get(1);
				String sale = fileContents.get(2);

				// エラーメッセージ：売上ファイルのフォーマットが不正な場合
				String fileName = rcdFiles.get(i).getName();

				if(fileContents.size() != 3) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}

				// エラーメッセージ：売上ファイルの支店コードが支店定義ファイルに存在しない場合
				if(!branchNames.containsKey(branchCode)) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				// エラーメッセージ：売上ファイルの商品コードが商品定義ファイルに存在しない場合
				if(!productNames.containsKey(productCode)) {
					System.out.println(fileName + "の商品コードが不正です");
					return;
				}

				// エラーメッセージ：売上金額が数字でない場合
				if(!sale.matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				// 売上金額（2行目）をlong型に変換
				long fileSale = Long.parseLong(sale);

				// branchSalesAmountに、取得した売上を加算
				Long branchSaleAmount = branchSales.get(branchCode) + fileSale;

				// productSalesAmountに、取得した売上を加算
				Long productSaleAmount = productSales.get(productCode) + fileSale;

				// エラーメッセージ：売上金額の合計が10桁を超えた場合
				if(branchSaleAmount >= 10000000000L || productSaleAmount >= 10000000000L) {
					System.out.println(SALE_AMOUNT_ERROR);
					return;
				}

				// Mapに値を保持する
				branchSales.put(branchCode, branchSaleAmount);
				productSales.put(productCode, productSaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, productNames, productSales)) {
			return;
		}

	}

	/**
	 * 支店定義・商品定義ファイルの読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String category, String regex) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			// エラーメッセージ：支店定義・商品定義ファイルが存在しない場合
			if(!file.exists()) {
				System.out.println(category + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {

				// 文字列を分割する
				String[] items = line.split(",");

				// エラーメッセージ：支店定義・商品定義ファイルのフォーマットが不正な場合
				if((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(category + FILE_INVALID_FORMAT);
					return false;
				}

				// Mapに値を保持する
				names.put(items[0], items[1]);
				sales.put(items[0], (long)0);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル・商品別集計ファイルの書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;

		try{

			File file = new File(path, fileName);
			bw = new BufferedWriter(new FileWriter(file));

			// Mapから全てのKeyを取得する
			for(String key: names.keySet()) {

				bw.write(key+ ", " + names.get(key) + ", " + sales.get(key));;
				// 改行のメソッド
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}

